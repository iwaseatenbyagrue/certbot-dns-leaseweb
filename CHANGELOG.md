# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [1.0.1] - 2023-10-21

* remove `zope.interface` dependency.

## [1.0.0] - 2023-10-21

* Maintenance update - migrate repo to use pyproject.toml, use Taskfile, test on multiple python versions.

## [0.0.3] - 2021-08-14

### Added

* This changelog.

### Modified

* Requesting certificates for arbitrarily deep subdomains is now supported.

## [0.0.2] - 2021-07-18

### Modified

* Fixed typo in setup.cfg bug tracker URL.

## [0.0.1] - 2021-07-18

Initial release for `certbot-dns-leaseweb`.

[0.0.3]: https://gitlab.com/iwaseatenbyagrue/certbot-dns-leaseweb/-/compare/v0.0.2...0.0.3
[0.0.2]: https://gitlab.com/iwaseatenbyagrue/certbot-dns-leaseweb/-/compare/v0.0.1...v0.0.2
[0.0.1]: https://gitlab.com/iwaseatenbyagrue/certbot-dns-leaseweb/-/tags/v0.0.1
